<!--?php

define("_SessionName", "messageBoard");
require("header.php");

$sql = "SELECT * FROM `message_board` ORDER BY `postTime` DESC";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        var_dump($row);
    }
}
else {
    echo "None.";
}
 
$conn->close();

?-->

<?php
header('Content-type: text/html; charset=UTF8');
?>

<html>  
<head>  
<link rel="stylesheet" type="text/css" href="style1.css">
</head>  

<?php

    define("_SessionName", "messageBoard");
    require("header.php");
    
    $sql = "SELECT * FROM `message_board` ORDER BY `postTime` DESC";
    
    $result = $conn->query($sql);

    session_start();

    date_default_timezone_set("prc");  

    //查询本页现实的数据 
    echo "<div style='margin-top:15px'>";
    while ($res = $result->fetch_array()) {
        $time = date("Y-m-d H:i:s", $res['postTime']);  

        echo "<div class='k'>";
        echo "<div class='ds-post-main'>";
        echo "<div class='ds-comment-body'>
        <span>{$res['authorName']}  于  {$time}  留言</span>
        <hr width=300px align=left> 
        <p>{$res['content']}</p></div><br>";
        echo "</div>";
        echo "</div>";
    }
    echo "</div>";

    echo "<br>";
    echo "<br>";
    echo "<br>";
    echo "</div>";
    
    $conn->close();

?>
</body>  
</html> 