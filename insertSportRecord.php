<?php

define("_SessionName", "insertSportRecord");
require("header.php");

$json = file_get_contents('php://input'); 
$data = json_decode($json, true);

// var_dump($data);

$sql = "INSERT INTO `sport_motion`(`master`, `distance`, `duration`, `pathLine`, 
`stratPoint`, `endPoint`, `mStartTime`, `mEndTime`, `calorie`, `speed`, 
`distribution`, `dateTag`, `str1`, `str2`, `str3`) VALUES 
({$data["master"]},{$data["distance"]},{$data["duration"]},'{$data["pathLine"]}','{$data["stratPoint"]}',
'{$data["endPoint"]}',{$data["mStartTime"]},{$data["mEndTime"]},{$data["calorie"]},{$data["speed"]},
{$data["distribution"]},'{$data["dateTag"]}','{$data["str1"]}','{$data["str2"]}','{$data["str3"]}')";

// echo $sql . "\n";

if ($conn->query($sql) === TRUE) {
    echo "Succeeded.";
} else {
    echo "Error: " . $sql . "\n" . $conn->error;
}
 
$conn->close();

?>