-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2021-12-08 15:00:45
-- 服务器版本： 5.5.62-log
-- PHP 版本： 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- 数据库： `myfit`
--
CREATE DATABASE IF NOT EXISTS `myfit` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `myfit`;

-- --------------------------------------------------------

--
-- 表的结构 `account`
--

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `account` varchar(50) NOT NULL,
  `psd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `message_board`
--

DROP TABLE IF EXISTS `message_board`;
CREATE TABLE `message_board` (
  `id` int(11) NOT NULL,
  `authorID` int(11) NOT NULL,
  `authorName` varchar(50) NOT NULL,
  `postTime` bigint(20) NOT NULL,
  `content` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `sport_motion`
--

DROP TABLE IF EXISTS `sport_motion`;
CREATE TABLE `sport_motion` (
  `id` int(11) NOT NULL,
  `master` int(11) NOT NULL,
  `distance` double NOT NULL,
  `duration` int(11) NOT NULL,
  `pathLine` mediumtext NOT NULL,
  `stratPoint` mediumtext NOT NULL,
  `endPoint` mediumtext NOT NULL,
  `mStartTime` bigint(20) NOT NULL,
  `mEndTime` bigint(20) NOT NULL,
  `calorie` double NOT NULL,
  `speed` double NOT NULL,
  `distribution` double NOT NULL,
  `dateTag` varchar(50) NOT NULL,
  `str1` mediumtext NOT NULL,
  `str2` mediumtext NOT NULL,
  `str3` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转储表的索引
--

--
-- 表的索引 `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account` (`account`);

--
-- 表的索引 `message_board`
--
ALTER TABLE `message_board`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sport_motion`
--
ALTER TABLE `sport_motion`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `message_board`
--
ALTER TABLE `message_board`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sport_motion`
--
ALTER TABLE `sport_motion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
